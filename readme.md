## Overview
---
> This repo hosts files for standing up a pihole server on an ubuntu host

## Setup
---
To be able to run the playbooks within this repo you need:

- Host with ansible installed
  - This is where you'll run the playbooks from

- Remote host running Debian based OS
  - Will expand in the future

## Execution
---
To run the playbook provided

```
ansible-playbook playbook.yml
```
> The playbook copies over your ssh key to known_hosts and also adds the current user to `/etc/sudo` file.  
> Therefore your first run will require you to pass the ssh pass and sudo pass.

```
ansible-playbook playbook.yml -b --ask-become-pass
```

Pi Hole container will instantiate with a random password.
You can check what the password is
```
docker logs <container_id> | grep random
```

You can also set a new password

```
# establish a shell with the container
docker exec -it <container> bash

# run this command to set new password
pihole -a -p
```


## Notes
---
> There will be some manual steps regarding disabling resolved on the remote host so that pi hole can use port 53 without issue.

Steps to disable resolved.
```
sudo systemctl disable systemd-resolved.service
sudo systemctl stop systemd-resolved
```

After setting up pi hole, set the dns server to itself
`/etc/netplan/00-installer-config.yaml`
```
# This is the network config written by 'subiquity'
network:
  ethernets:
    ens160:
      dhcp4: false
      addresses:
        - 192.168.1.XX/24
      gateway4: 192.168.1.1
      nameservers:
        addresses: [127.0.0.1, 192.168.1.1]
  version: 2
```

Apply the new settings
```
sudo netplan apply
```